package com.company;

public class Main {

    public static void main(String[] args) {
        String originalText = "А:рген-тина ,мАн.ит Не?гра!";
        originalText = originalText.replaceAll("[,! .?:-]", "");
        StringBuffer originalTextBuffer = new StringBuffer(originalText);
        String turnedText = new String(originalTextBuffer.reverse());
        if (originalText.equalsIgnoreCase(turnedText))
            System.out.println("Палиндром");
        else {
            System.out.println("Не палиндром");
        }
    }
}
